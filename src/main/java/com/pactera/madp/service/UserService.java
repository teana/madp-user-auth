package com.pactera.madp.service;

import com.pactera.madp.model.dto.UserDto;
import com.pactera.madp.model.entity.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

public interface UserService {

	UserDto login(UserDto userDto);

	UserDto info(String id);

	Boolean logout(String id);
}
