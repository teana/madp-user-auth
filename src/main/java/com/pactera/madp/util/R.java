package com.pactera.madp.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/16 上午8:33
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class R<T> {
    public static final Integer SUCCESS = 1;
    public static final Integer FAIL = -1;
    private Integer code;
    private String msg;
    private T data;

    /****************************************/
    public static R ok(){
        return R.ok(null);
    }

    public static R ok(Object data){
        R r = new R<>();
        r.setCode(SUCCESS);
        r.setMsg("SUCCESS");
        r.setData(data);
        return r;
    }

    public static R error(String msg){
        R r = new R<>();
        r.setCode(FAIL);
        r.setMsg(msg);
        return r;
    }

    public static R error(Exception e){
       return  R.error(e.getMessage());
    }


    /****************************************/
}
