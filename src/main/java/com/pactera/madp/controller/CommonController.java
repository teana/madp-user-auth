package com.pactera.madp.controller;

import com.pactera.madp.util.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/25 下午8:47
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description: 通用控制器
 */
@RestController
@RequestMapping("/common")
public class CommonController {

    /**
     * 获取服务器时间戳（主要用途：计算客户端与服务端的时间差）
     * @return
     */
    @GetMapping("/timestamp")
    public R<Long> timestamp() {
        return R.ok(System.currentTimeMillis());
    }
}
