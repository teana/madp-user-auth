package com.pactera.madp.controller;

import cn.hutool.http.HttpStatus;
import com.pactera.madp.model.dto.UserDto;
import com.pactera.madp.util.R;
import com.pactera.madp.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.pactera.madp.service.UserService;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;

	@PostMapping("/login")//@RequestBody 需要优化验签
	public R<UserDto> login(@RequestBody UserDto userDto){
		UserDto user = userService.login(userDto);
		if(user != null){
			return R.ok(user);
		}
		WebUtils.getResponse().setStatus(HttpStatus.HTTP_BAD_REQUEST);
		return R.error("用户名不存在或者密码错误");
	}

	@GetMapping
	public R<UserDto> info(@RequestParam Map<String, Object> params) {
		String id = (String) params.get("id");
		return R.ok(userService.info(id));
	}

	@DeleteMapping
	public R<Boolean> logout(@RequestParam String id) {
		return R.ok(userService.logout(id));
	}
}
