package com.pactera.madp.constant;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/20 上午2:19
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description:
 */
public interface SecurityConstants {

    /**
     * 前缀
     */
    String MADP_PREFIX = "madp_";

    /**
     * oauth 相关前缀
     */
    String OAUTH_PREFIX = "oauth_";

    /**
     * token 前缀
     */
    String TOKEN_PREFIX = MADP_PREFIX + OAUTH_PREFIX + "token:";

    /**
     * 防重 前缀
     */
    String NONCE_PREFIX = MADP_PREFIX + OAUTH_PREFIX + "nonce:";

    /**
     *
     */
    String USER_ID_PREFIX = MADP_PREFIX + OAUTH_PREFIX + "userid:";
}
