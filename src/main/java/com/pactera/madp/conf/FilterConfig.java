package com.pactera.madp.conf;

import com.pactera.madp.filter.AuthFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/18 上午1:04
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description:
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean noRepeatFilter(){
        Map<String, String> params = new HashMap<>();
        params.put("noRepeatTime", "60000");

        FilterRegistrationBean registration = new FilterRegistrationBean(new AuthFilter());
        registration.addUrlPatterns("/*");
        registration.setName("authFilter");
        registration.setInitParameters(params);
        registration.setOrder(1);
        return registration;
    }

}
