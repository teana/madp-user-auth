package com.pactera.madp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/17 下午7:54
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description:
 */
@ServletComponentScan
@SpringBootApplication
public class MadpUserAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(MadpUserAuthApplication.class, args);
    }
}
