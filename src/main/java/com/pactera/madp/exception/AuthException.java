package com.pactera.madp.exception;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/18 上午12:34
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description: 自定义异常（安全认证）
 */
public class AuthException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public static final String REQUEST_BODY_NOT_JSON = "Request body not json";

    public AuthException() {
    }

    public AuthException(String message) {
        super(message);
    }

    public AuthException(Throwable cause) {
        super(cause);
    }

    public AuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}