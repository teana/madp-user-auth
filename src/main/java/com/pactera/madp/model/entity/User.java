package com.pactera.madp.model.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String id;
	private String username;
	private String password;
}
