package com.pactera.madp.model.dto;

import com.pactera.madp.model.entity.User;
import lombok.Data;

/**
 * All rights Reserved, Designed By teana@sina.cn.
 *
 * @Author: Deming.Chang
 * @Email: teana@sina.cn
 * @Date: 2018/9/16 上午9:37
 * @Version: 1.0.0
 * @Copyright: 2018
 * @Description:
 */
@Data
public class UserDto extends User {

    private String token;
}
